# Interactive Panels

A plugin for creating interactive panels in Minecraft in the form of images rendered on maps hanging on item frames, with the option to add customizable buttons to them.
This uses fake entities and packets to avoid lags and allow for panels to be shown to specific players exclusively.

It was primarily developed and ported as a module for the game framework of [GommeHD.net](https://gommehd.net)

Check it out in this [video](https://www.youtube.com/watch?v=m0ThVTJtmJE)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

Since this relies on Minecraft protocol hooks, it requires the [ProtocolLib](https://www.spigotmc.org/resources/protocollib.1997/)

## Authors

* **Dosenwerfer** - *Main development* - [Dosenwerfer](https://gitlab.com/Dosenwerfer)
* **Overload** - *Consulting* - [Maniload](https://gitlab.com/Maniload)/[Overload](https://www.gommehd.net/forum/members/overload.40403/)

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
