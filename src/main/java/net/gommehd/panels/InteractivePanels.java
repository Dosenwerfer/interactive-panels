package net.gommehd.panels;

import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.dosenwerfer.minecraftcommons.particles.Particle;
import lombok.Getter;
import net.cubespace.Yamler.Config.InvalidConfigurationException;
import net.gommehd.panels.api.Button;
import net.gommehd.panels.api.Panel;
import net.gommehd.panels.api.PanelManager;
import net.gommehd.panels.api.PanelVector;
import net.gommehd.panels.config.PanelConfig;
import net.gommehd.panels.config.PanelsConfig;
import net.gommehd.panels.listeners.PlayerInteractListener;
import net.gommehd.panels.listeners.PlayerJoinListener;
import net.gommehd.panels.runnables.PanelViewCheck;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.plugin.java.JavaPlugin;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * InteractivePanels v1.0
 *
 * TODO documentation
 * TODO goframe module
 *
 * @author Dosenwerfer (01-Oct-2017)
 */
public class InteractivePanels extends JavaPlugin {

    private static @Getter InteractivePanels instance;
    private static @Getter ProtocolManager protocolManager;
    private static @Getter PanelManager panelManager;
    private static @Getter PanelsConfig panelsConfig;
    private int viewCheckInterval;

    public void onEnable() {
        instance = this;

        // Initiate ProtocolLib
        protocolManager = ProtocolLibrary.getProtocolManager();

        // Initiate PanelManager
        panelManager = PanelManager.getInstance();

        // Initiate config
        panelsConfig = new PanelsConfig();
        int viewCheckInterval = panelsConfig.getInterval();
        this.viewCheckInterval = viewCheckInterval > 1 ? viewCheckInterval : 1;
        for (PanelConfig panelConfig : panelsConfig.getPanels().values()) {
            String name = panelConfig.getName();
            try {
                BufferedImage image;
                if (panelConfig.getFile() != null) {
                    image = ImageIO.read(new File(getDataFolder() + File.separator + panelConfig.getFile()));
                } else if (panelConfig.getUrl() != null) {
                    image = ImageIO.read(new URL(panelConfig.getUrl()));
                } else {
                    throw new InvalidConfigurationException("No image path or URL specified for " + name + "!");
                }
                Panel panel = new Panel(name,
                        panelConfig.getSupportBlock().toLocation().getBlock(),
                        Panel.PanelFace.valueOf(panelConfig.getFace()),
                        image,
                        panelConfig.getTriggerDistance());
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }

        // Register listeners
        Bukkit.getPluginManager().registerEvents(new PlayerInteractListener(), this);
        Bukkit.getPluginManager().registerEvents(new PlayerJoinListener(), this);

        // Start panel view check
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new PanelViewCheck(), 0, viewCheckInterval);

        // Dummy
        dummy();
        halloween();
    }

    public void onDisable() {
        // Save config
        /*try {
            panelsConfig.save();
        } catch (InvalidConfigurationException e) {
            e.printStackTrace();
        }*/
    }

    private void dummy() {
        Panel dummy = panelManager.getPanel("dummy");
        if (dummy != null) {
            dummy.addButton(new Button(dummy, new PanelVector(80, 590), 560, 130).setClickHandler((button, player) -> {
                player.sendMessage("§7[§3InteractivePanels§7] Sending you to §3BedWars§7...");
            }).setParticle(Particle.FLAME).setSound(Sound.CAT_MEOW));
            dummy.addButton(new Button(dummy, new PanelVector(1390, 400), 550, 230).setClickHandler((button, player) -> {
                player.sendMessage("§7[§3InteractivePanels§7] Switching to §3night§7...");
                Location loc = dummy.toBukkitLocation(button.getStart()).add(dummy.getFace().getX(), 0, dummy.getFace().getZ());
                dummy.getWorld().spigot().strikeLightningEffect(loc, false);
                dummy.getWorld().setTime(21000);
            }).setParticle(Particle.FLAME).setSound(Sound.CAT_MEOW));
        }
    }

    private void halloween() {
        Panel halloween = panelManager.getPanel("halloween");
        if (halloween != null) {
            halloween.addButton(new Button(halloween, new PanelVector(0, 0), 16 * 128 / 2, 9 * 128).setClickHandler((button, player) -> {
                player.sendMessage("§7[§3InteractivePanels§7] Switching to §8BLACK§7...");
                try {
                    halloween.setImage(ImageIO.read(new URL("https://vignette.wikia.nocookie.net/joke-battles/images/5/5a/Black.jpg")));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }).setParticle(Particle.FLAME).setSound(Sound.CAT_MEOW));
            halloween.addButton(new Button(halloween, new PanelVector(16 * 128 / 2, 0), 16 * 128 / 2, 9 * 128).setClickHandler((button, player) -> {
                player.sendMessage("§7[§3InteractivePanels§7] Switching to §fWHITE§7...");
                try {
                    halloween.setImage(ImageIO.read(new URL("https://i.ytimg.com/vi/br1xloaJROw/maxresdefault.jpg")));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }).setParticle(Particle.FLAME).setSound(Sound.CAT_MEOW));
        }
    }
}
