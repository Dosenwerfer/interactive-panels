package net.gommehd.panels.api;

import com.google.common.collect.Maps;
import net.gommehd.panels.InteractivePanels;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Dosenwerfer
 */
public class PanelManager {

    private static PanelManager instance;
    private final Map<String, Panel> panelMap = Maps.newConcurrentMap();

    private PanelManager() {
        // Keep alive task for item frames
        Bukkit.getScheduler().runTaskTimer(InteractivePanels.getInstance(), () -> {
            for (Panel panel : panelMap.values()) {
                panel.send();
            }
        }, 20L * 20L, 20L * 20L);
    }

    public static PanelManager getInstance() {
        if (instance == null) {
            instance = new PanelManager();
        }
        return instance;
    }

    public void addPanel(Panel panel) {
        panelMap.put(panel.getName(), panel);
    }

    public void untrack(String name) {
        panelMap.remove(name);
    }

    public void untrack(Panel panel) {
        panelMap.remove(panel.getName());
    }

    public Panel getPanel(String name) {
        return panelMap.get(name);
    }

    public Collection<Panel> getPanels() {
        return panelMap.values();
    }

    public Set<Panel> getPanels(Player player) {
        return getPanels().stream().filter(panel -> panel.isSubscribed(player)).collect(Collectors.toSet());
    }
}
