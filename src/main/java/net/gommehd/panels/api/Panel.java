package net.gommehd.panels.api;

import com.comphenix.packetwrapper.WrapperPlayServerEntityDestroy;
import com.google.common.collect.Sets;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import net.gommehd.panels.InteractivePanels;
import net.gommehd.panels.utils.Algebra;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.awt.image.BufferedImage;
import java.awt.image.RasterFormatException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * TODO documentation
 * @author Dosenwerfer
 */
@Getter
public class Panel {

    private final String name;
    private final boolean real;
    private final Collection<Player> players;

    private final Block supportBlock;
    private final int width, height;
    private final PanelFace face;
    private BufferedImage image;
    private @Setter double triggerDistance;

    private final World world;
    private final Location panelStart, panelEnd;
    private final ArrayList<PanelFrame> panelFrames = new ArrayList<>();
    private final Set<Button> buttons = new HashSet<>();

    private final WrapperPlayServerEntityDestroy packetWrapperDestroy;

    public Panel(String name, Block supportBlock, PanelFace face, BufferedImage image) {
        this(name, supportBlock, face, image, 10);
    }

    public Panel(String name, Block supportBlock, PanelFace face, BufferedImage image, double triggerDistance) {
        this(name, true, null, supportBlock, face, image, triggerDistance);
    }

    public Panel(String name, Player player, Block supportBlock, PanelFace face, BufferedImage image) {
        this(name, player, supportBlock, face, image, 10);
    }

    public Panel(String name, Player player, Block supportBlock, PanelFace face, BufferedImage image, double triggerDistance) {
        this(name, false, Sets.newConcurrentHashSet(Sets.newHashSet(player)), supportBlock, face, image, triggerDistance);
    }

    public Panel(String name, Collection<Player> players, Block supportBlock, PanelFace face, BufferedImage image) {
        this(name, false, players, supportBlock, face, image, 10);
    }

    public Panel(@NonNull String name, boolean real, Collection<Player> players, @NonNull Block supportBlock, @NonNull PanelFace face, @NonNull BufferedImage image, double triggerDistance) {
        this.name = name;
        this.real = real;
        this.players = players;
        this.supportBlock = supportBlock;
        this.width = image.getWidth() / 128;
        this.height = image.getHeight() / 128;
        this.face = face;
        this.image = image;
        this.triggerDistance = triggerDistance;
        this.world = supportBlock.getWorld();

        // Log
        InteractivePanels.getInstance().getLogger().info("Building panel \"" + name + "\" at x: " + supportBlock.getX() + " y: " + supportBlock.getY() + " z: " + supportBlock.getZ() + " facing " + face.toString() + "...");

        // Determine corner positions
        Block supportBlockEnd = supportBlock.getLocation().add(width * face.getPrevious().getX(), -height, width * face.getPrevious().getZ()).getBlock();
        switch (face) {
            case EAST:
                panelStart = supportBlock.getLocation().add(1.09, 1, 1);
                panelEnd = supportBlockEnd.getLocation().add(1.09, 0, 0);
                break;
            case SOUTH:
                panelStart = supportBlock.getLocation().add(0, 1, 1.09);
                panelEnd = supportBlockEnd.getLocation().add(1, 0, 1.09);
                break;
            case WEST:
                panelStart = supportBlock.getLocation().add(-0.09, 1, 0);
                panelEnd = supportBlockEnd.getLocation().add(-0.09, 0, 1);
                break;
            case NORTH:
                panelStart = supportBlock.getLocation().add(1, 1, -0.09);
                panelEnd = supportBlockEnd.getLocation().add(0, 0, -0.09);
                break;
            default:
                throw new IllegalArgumentException("Illegal PanelFace!"); // Cannot happen
        }

        // Create panel frames
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                panelFrames.add(new PanelFrame(this, x, y));
            }
        }

        // Create destroy packet
        this.packetWrapperDestroy = new WrapperPlayServerEntityDestroy();
        int[] entityIds = new int[panelFrames.size()];
        for (int i = 0; i < panelFrames.size(); i++) {
            entityIds[i] = panelFrames.get(i).getEntityId();
        }
        packetWrapperDestroy.setEntityIds(entityIds);

        InteractivePanels.getPanelManager().addPanel(this);
    }

    public void addPlayer(Player player) {
        send(player);
        players.add(player);
    }

    public void removePlayer(Player player) {
        players.remove(player);
        packetWrapperDestroy.sendPacket(player);

        // Remove player metadata
        if (player.hasMetadata("panel")) {
            if (player.getMetadata("panel").get(0).value() == this) {
                player.removeMetadata("panel", InteractivePanels.getInstance());
            }
        }
        if (player.hasMetadata("panelButtonHover")) {
            for (Button button : buttons) {
                if (player.getMetadata("panelButtonHover").get(0).value() == button) {
                    player.removeMetadata("panelButtonHover", InteractivePanels.getInstance());
                    break;
                }
            }
        }
    }

    public Collection<Player> getPlayers() {
        return real ? (Collection<Player>) Bukkit.getOnlinePlayers() : players;
    }

    public void send() {
        for (Player player : getPlayers()) {
            for (PanelFrame panelFrame : panelFrames) {
                panelFrame.send(player);
            }
        }
    }

    public void send(Player player) {
        for (PanelFrame panelFrame : panelFrames) {
            panelFrame.send(player);
        }
    }

    public void sendEntities() {
        for (Player player : getPlayers()) {
            for (PanelFrame panelFrame : panelFrames) {
                panelFrame.sendEntity(player);
            }
        }
    }

    public void remove() {
        // Untrack panel
        InteractivePanels.getPanelManager().untrack(this);
        // Send remove packets for frames
        for (Player player : getPlayers()) {
            packetWrapperDestroy.sendPacket(player);
        }
    }

    public void addButton(Button button) {
        buttons.add(button);
    }

    public void removeButton(Button button) {
        buttons.remove(button);
    }

    public void setImage(BufferedImage image) {
        this.image = image;
        Bukkit.getScheduler().runTaskAsynchronously(InteractivePanels.getInstance(), () -> {
            // Refresh frames
            boolean rfe = false;
            for (PanelFrame panelFrame : panelFrames) {
                try {
                    panelFrame.refresh();
                } catch (RasterFormatException e) {
                    rfe = true;
                } finally {
                    // Send new image data
                    for (Player player : getPlayers()) {
                        panelFrame.sendData(player);
                    }
                }
            }
            if (rfe) {
                InteractivePanels.getInstance().getLogger().warning("The provided image in panel " + name + " is smaller (" + image.getWidth() + "x" + image.getHeight() + ") than its original image (" + width * 128 + "x" + height * 128 + ")!");
            }
        });
    }

    public Location toBukkitLocation(final PanelVector vector) {
        double dxz = vector.getX() / 128;
        return panelStart.clone().add(dxz * face.getPrevious().getX(), -vector.getY() / 128, dxz * face.getPrevious().getZ());
    }

    /**
     * @param entity LivingEntity
     * @return PanelVector representing the point of the panel, the entity is looking at,
     * or null if it is not looking at the panel.
     */
    public PanelVector getViewPoint(LivingEntity entity) {
        // Line
        Vector p = entity.getEyeLocation().toVector();
        Vector a = entity.getEyeLocation().getDirection();

        // Plane
        Vector q = panelStart.toVector();
        Vector b = new Vector(width * face.getPrevious().getX(), 0, width * face.getPrevious().getZ());
        Vector c = new Vector(0, -height, 0);

        // Prepare system of linear equations
        double[][] lgs = {
                {a.getX(), -b.getX(), -c.getX(), q.getX() - p.getX()},
                {a.getY(), -b.getY(), -c.getY(), q.getY() - p.getY()},
                {a.getZ(), -b.getZ(), -c.getZ(), q.getZ() - p.getZ()}
        };

        // Solve system of linear equations
        Double[] result = Algebra.linSolve(lgs);
        if (result == null) {
            return null;
        }
        double r = result[0];
        double s = result[1];
        double t = result[2];

        // Ensure that player looks at panel at all
        if (r < 0 || s < 0 || s > 1 || t < 0 || t > 1) {
            return null;
        }
        Vector br = new Vector(s * b.getX(), 0, s * b.getZ());
        return new PanelVector(br.length() * 128, t * -c.getY() * 128);
    }

    public boolean isSubscribed(Player player) {
        return getPlayers().contains(player);
    }

    public boolean isInteractiveFrom(Location location) {
        return isInRange(location, triggerDistance);
    }

    private boolean isInRange(Location location, double distance) {
        // Check if location is in same world
        if (location.getWorld() != world) {
            return false;
        }
        // Check if location is on the "correct" side of the panel
        if (face.getZ() == 0) {
            if ((location.getX() - panelStart.getX()) * face.getX() < 0) {
                return false;
            }
        } else {
            if ((location.getZ() - panelStart.getZ()) * face.getZ() < 0) {
                return false;
            }
        }
        Vector center = panelStart.toVector().add(panelEnd.toVector()).multiply(0.5);
        return center.distanceSquared(location.toVector()) <= distance * distance;
    }

    @Getter
    @AllArgsConstructor
    public enum PanelFace {
        EAST(1, 0, "NORTH", 3), SOUTH(0, 1, "EAST", 0), WEST(-1, 0, "SOUTH", 1), NORTH(0, -1, "WEST", 2);

        private final int x, z;
        private final String previous;
        private final int objectData; // inverted

        public PanelFace getPrevious() {
            return PanelFace.valueOf(previous);
        }
    }
}
