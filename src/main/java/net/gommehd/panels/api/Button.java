package net.gommehd.panels.api;

import com.dosenwerfer.minecraftcommons.particles.Particle;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import net.gommehd.panels.InteractivePanels;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;

/**
 * @author Dosenwerfer
 */
@Getter
public class Button {

    private final Panel panel;
    private @Setter PanelVector start;
    private @Setter int width, height;
    private ButtonHoverEventHandler hoverHandler;
    private ButtonClickEventHandler clickHandler;
    private double triggerDistance = 10;
    private Particle particle;
    private Sound sound;

    public Button(@NonNull Panel panel, @NonNull PanelVector start, int width, int height) {
        this.panel = panel;
        this.start = start;
        this.width = width;
        this.height = height;
    }

    public boolean contains(PanelVector vector) {
        // DEBUG System.out.println("[DEBUG] CONTAINS vector: " + vector.getX() + " " + vector.getY() + " start: " + start.getX() + " " + start.getY() + " width: " + width + " height: " + height);
        return vector.getX() >= start.getX() && vector.getX() <= start.getX() + width
                && vector.getY() >= start.getY() && vector.getY() <= start.getY() + height;
    }

    public void hover(Player player, PanelVector vector) {
        player.setMetadata("panelButtonHover", new FixedMetadataValue(InteractivePanels.getInstance(), this));
        if (particle != null) {
            particle.display(0, 0, 0, 0, 1, panel.toBukkitLocation(start), player);
            particle.display(0, 0, 0, 0, 1, panel.toBukkitLocation(start.clone().add(width, 0)), player);
            particle.display(0, 0, 0, 0, 1, panel.toBukkitLocation(start.clone().add(0, height)), player);
            particle.display(0, 0, 0, 0, 1, panel.toBukkitLocation(start.clone().add(width, height)), player);
        }
        if (sound != null) {
            player.playSound(player.getLocation(), sound, 1, 1);
        }
        if (hoverHandler != null) {
            hoverHandler.onButtonHover(this, player, vector);
        }
    }

    public void click(Player player) {
        player.setMetadata("panelButtonClick", new FixedMetadataValue(InteractivePanels.getInstance(), this));
        if (clickHandler != null) {
            clickHandler.onButtonClick(this, player);
        }
    }

    // --------- Interfaces ---------

    public interface ButtonHoverEventHandler {
        void onButtonHover(Button button, Player player, PanelVector vector);
    }

    public interface ButtonClickEventHandler {
        void onButtonClick(Button button, Player player);
    }

    // --------- Builder ---------

    public Button setHoverHandler(ButtonHoverEventHandler hoverHandler) {
        this.hoverHandler = hoverHandler;
        return this;
    }

    public Button setClickHandler(ButtonClickEventHandler clickHandler) {
        this.clickHandler = clickHandler;
        return this;
    }

    public Button setTriggerDistance(double triggerDistance) {
        this.triggerDistance = triggerDistance;
        return this;
    }

    public Button setParticle(Particle particle) {
        this.particle = particle;
        return this;
    }

    public Button setSound(Sound sound) {
        this.sound = sound;
        return this;
    }
}
