package net.gommehd.panels.api;

import com.comphenix.packetwrapper.WrapperPlayServerEntityHeadRotation;
import com.comphenix.packetwrapper.WrapperPlayServerEntityMetadata;
import com.comphenix.packetwrapper.WrapperPlayServerSpawnEntity;
import com.comphenix.protocol.wrappers.WrappedWatchableObject;
import lombok.Getter;
import net.minecraft.server.v1_8_R3.PacketPlayOutMap;
import net.minecraft.server.v1_8_R3.WorldMap;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_8_R3.map.CraftMapView;
import org.bukkit.craftbukkit.v1_8_R3.map.RenderData;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.map.MapCanvas;
import org.bukkit.map.MapRenderer;
import org.bukkit.map.MapView;

import java.awt.image.RasterFormatException;
import java.util.ArrayList;
import java.util.Collections;

/**
 * @author Dosenwerfer, Overload
 */
public class PanelFrame {

    private static int entityCount = 11000;

    private final Panel panel;
    private final @Getter int x, y;
    private final Location location;
    private final MapView mapView;
    private final @Getter int entityId;

    private PacketPlayOutMap packetMapData;
    private final WrapperPlayServerSpawnEntity packetWrapperSpawnEntity;
    private final WrapperPlayServerEntityHeadRotation packetWrapperHeadRotation;
    private final WrapperPlayServerEntityMetadata packetWrapperEntityMetadata;

    public PanelFrame(Panel panel, int x, int y) {
        this.panel = panel;
        this.x = x;
        this.y = y;
        this.location = panel.getSupportBlock().getRelative(
                panel.getFace().getX() + panel.getFace().getPrevious().getX() * x,
                -y,
                panel.getFace().getZ() + panel.getFace().getPrevious().getZ() * x
        ).getLocation();
        this.entityId = entityCount++;

        // Create MapView
        this.mapView = new WorldMap("map_" + entityId).mapView;

        // Create map packet
        refresh();

        // Create spawn packet
        packetWrapperSpawnEntity = new WrapperPlayServerSpawnEntity();
        packetWrapperSpawnEntity.setType(WrapperPlayServerSpawnEntity.ObjectTypes.ITEM_FRAME);
        packetWrapperSpawnEntity.setEntityID(entityId);
        packetWrapperSpawnEntity.setX(location.getX());
        packetWrapperSpawnEntity.setY(location.getY());
        packetWrapperSpawnEntity.setZ(location.getZ());
        packetWrapperSpawnEntity.setObjectData(panel.getFace().getObjectData());
        packetWrapperSpawnEntity.setPitch(panel.getFace().getObjectData() * 90f);

        // Create head rotation packet
        packetWrapperHeadRotation = new WrapperPlayServerEntityHeadRotation();
        packetWrapperHeadRotation.setEntityID(entityId);
        packetWrapperHeadRotation.setHeadYaw((byte) 0);

        // Create metadata packet
        packetWrapperEntityMetadata = new WrapperPlayServerEntityMetadata();
        packetWrapperEntityMetadata.setEntityID(entityId);
        packetWrapperEntityMetadata.setMetadata(Collections.singletonList(new WrappedWatchableObject(8, new ItemStack(Material.MAP, 1, mapView.getId()))));
    }

    public void refresh() throws RasterFormatException {
        for (MapRenderer renderer : mapView.getRenderers()) {
            mapView.removeRenderer(renderer);
        }

        final int finalX = x, finalY = y;
        mapView.addRenderer(new MapRenderer() {
            @Override
            public void render(MapView mapView, MapCanvas mapCanvas, Player player) {
                mapCanvas.drawImage(0, 0,
                        panel.getImage().getSubimage(
                                finalX * 128,
                                finalY * 128,
                                Math.min(128, panel.getImage().getWidth() - finalX * 128),
                                Math.min(128, panel.getImage().getHeight() - finalY * 128)
                        )
                );
            }
        });

        // Create map packet
        RenderData renderData = ((CraftMapView) mapView).render(null);
        packetMapData = new PacketPlayOutMap(mapView.getId(), mapView.getScale().getValue(), new ArrayList<>(), renderData.buffer, 0, 0, 128, 128);
    }

    public void send(Player player) {
        sendData(player);
        sendEntity(player);
    }

    public void sendData(Player player) {
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(packetMapData);
    }

    public void sendEntity(Player player) {
        packetWrapperSpawnEntity.sendPacket(player);
        packetWrapperHeadRotation.sendPacket(player);
        packetWrapperEntityMetadata.sendPacket(player);
    }
}
