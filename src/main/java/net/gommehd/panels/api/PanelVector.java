package net.gommehd.panels.api;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author Dosenwerfer
 */
@AllArgsConstructor
public class PanelVector implements Cloneable {

    private @Getter double x, y;

    public PanelVector add(double x, double y) {
        this.x += x;
        this.y += y;
        return this;
    }

    public PanelVector subtract(double x, double y) {
        this.x -= x;
        this.y -= y;
        return this;
    }

    public PanelVector add(PanelVector vector) {
        this.x += vector.x;
        this.y += vector.y;
        return this;
    }

    public PanelVector subtract(PanelVector vector) {
        this.x -= vector.x;
        this.y -= vector.y;
        return this;
    }

    public PanelVector setX(double x) {
        this.x = x;
        return this;
    }

    public PanelVector setY(double y) {
        this.y = y;
        return this;
    }

    @Override
    public PanelVector clone() {
        try {
            return (PanelVector) super.clone();
        } catch (CloneNotSupportedException exeption) {
            throw new Error(exeption);
        }
    }
}
