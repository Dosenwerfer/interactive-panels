package net.gommehd.panels.runnables;

import net.gommehd.panels.InteractivePanels;
import net.gommehd.panels.api.Button;
import net.gommehd.panels.api.Panel;
import net.gommehd.panels.api.PanelVector;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;

/**
 * @author Dosenwerfer
 */
public class PanelViewCheck implements Runnable {

    @Override
    public void run() {
        for (Player player : Bukkit.getOnlinePlayers()) {
            boolean panelDetected = false;
            boolean buttonDetected = false;
            for (Panel panel : InteractivePanels.getPanelManager().getPanels(player)) {
                if (panel.isInteractiveFrom(player.getEyeLocation())) {
                    PanelVector vector = panel.getViewPoint(player);
                    if (vector == null) {
                        continue;
                    }

                    panelDetected = true;
                    player.setMetadata("panel", new FixedMetadataValue(InteractivePanels.getInstance(), panel));

                    // Trigger applicable panel buttons
                    for (Button button : panel.getButtons()) {
                        if (button.contains(vector)) {
                            buttonDetected = true;
                            if (!player.hasMetadata("panelButtonHover") || player.getMetadata("panelButtonHover").get(0).value() != button) {
                                button.hover(player, vector);
                            }
                        }
                    }
                }
            }
            if (!panelDetected) {
                player.removeMetadata("panel", InteractivePanels.getInstance());
            }
            if (!buttonDetected) {
                player.removeMetadata("panelButtonHover", InteractivePanels.getInstance());
            }
        }
    }
}
