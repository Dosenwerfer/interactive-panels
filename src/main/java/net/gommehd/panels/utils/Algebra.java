/* License

Copyright (c) 2017 Dosenwerfer

The code contained in this file may be used free of charge, though not
exclusively, and/or altered by GommeHD.net GmbH under the condition of
attribution to the original author (Dosenwerfer). To comply with this
condition, JavaDoc "@author" tags may not be altered or removed.
Furthermore, the project, that this file is part of, must contain
a reference to the original author of this file in its documentation.

THE CODE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

This license text may not be removed by any means and shall be included in all
copies or substantial portions of this file.
*/

package net.gommehd.panels.utils;

/**
 * @author Dosenwerfer (https://dosenwerfer.com)
 */
public class Algebra {

    /**
     * Solves a cubic system of linear equations using Gaussian elimination.
     *
     * @param lgs Cubic system of linear equations: [Equations 0...n [Variables x_0...x_n + Constant]]
     * @return Double array with results for unknowns or null if the equations cannot be solved distinctly
     * @throws IllegalArgumentException thrown when the system of equations is not cubic
     */
    public static Double[] linSolve(double[][] lgs) throws IllegalArgumentException {
        int n = lgs.length;
        if (n == 0 || n + 1 != lgs[0].length) {
            throw new IllegalArgumentException("Number of equations does not match number of variables!");
        }
        for (int i = 0; i < n; i++) {
            if (lgs[i][i] == 0) {
                int equation = -1;
                for (int j = 0; j < n; j++) {
                    if (lgs[j][i] != 0) {
                        equation = j;
                        break;
                    }
                }
                if (equation == -1) {
                    return null;
                }
                double[] temp = lgs[equation];
                lgs[equation] = lgs[i];
                lgs[i] = temp;
            }
            double[] coefficients = lgs[i];
            double diag = coefficients[i];
            for (int j = 0; j <= n; j++) {
                coefficients[j] /= diag;
            }
            for (int j = (i == 0 ? 1 : 0); j < n; j += (j + 1 == i ? 2 : 1)) {
                double[] nCoefficients = lgs[j];
                double multiplier = -nCoefficients[i];
                for (int k = 0; k <= n; k++) {
                    nCoefficients[k] += multiplier * coefficients[k];
                }
            }
        }
        Double[] result = new Double[n];
        for (int i = 0; i < n; i++) {
            result[i] = lgs[i][n];
        }
        return result;
    }
}
