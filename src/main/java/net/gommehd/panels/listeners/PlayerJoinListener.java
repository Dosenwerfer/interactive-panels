package net.gommehd.panels.listeners;

import net.gommehd.panels.InteractivePanels;
import net.gommehd.panels.api.Panel;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 * @author Dosenwerfer
 */
public class PlayerJoinListener implements Listener {

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        for (Panel panel : InteractivePanels.getPanelManager().getPanels(event.getPlayer())) {
            panel.send(event.getPlayer());
        }
    }
}
