package net.gommehd.panels.listeners;

import net.gommehd.panels.api.Button;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

/**
 * @author Dosenwerfer
 */
public class PlayerInteractListener implements Listener {

    @EventHandler(priority = EventPriority.LOW)
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (event.getAction() == Action.PHYSICAL) {
            return;
        }
        Player player = event.getPlayer();

        if (!player.hasMetadata("panelButtonHover")) {
            return;
        }

        Button button = (Button) player.getMetadata("panelButtonHover").get(0).value();
        button.click(player);

        event.setCancelled(true);
    }
}
