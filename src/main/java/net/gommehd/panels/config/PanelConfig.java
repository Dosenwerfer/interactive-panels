package net.gommehd.panels.config;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.cubespace.Yamler.Config.Comment;
import net.cubespace.Yamler.Config.YamlConfig;

/**
 * @author Dosenwerfer
 */
@Data
@EqualsAndHashCode( callSuper = false )
public class PanelConfig extends YamlConfig {

    @Comment("Unique identifier")
    private String name = "dummy";
    @Comment("Most upper left block the panel will be hanging on")
    private BlockConfig supportBlock;
    @Comment("Direction the panel faces: EAST, SOUTH, WEST, NORTH")
    private String face;
    @Comment("Local image file")
    private String file = null;
    @Comment("Image from the internet")
    private String url = null;
    @Comment("Range players have to be in for the panel controls to trigger (could influence performance, because of complex calculations)")
    private double triggerDistance = 10;
}
