package net.gommehd.panels.config;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.cubespace.Yamler.Config.YamlConfig;
import org.bukkit.Bukkit;
import org.bukkit.Location;

/**
 * @author Dosenwerfer
 */
@Data
@EqualsAndHashCode( callSuper = false )
public class BlockConfig extends YamlConfig {

    private String world;
    private double x, y, z;

    public Location toLocation() {
        return new Location(Bukkit.getWorld(world), x, y, z);
    }
}
