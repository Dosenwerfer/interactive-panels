package net.gommehd.panels.config;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.cubespace.Yamler.Config.Comment;
import net.cubespace.Yamler.Config.InvalidConfigurationException;
import net.cubespace.Yamler.Config.YamlConfig;
import net.gommehd.panels.InteractivePanels;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Dosenwerfer
 */
@Data
@EqualsAndHashCode( callSuper = false )
public class PanelsConfig extends YamlConfig {

    public PanelsConfig() {
        CONFIG_HEADER = new String[] {"InteractivePanels configuration"};
        CONFIG_FILE = new File(InteractivePanels.getInstance().getDataFolder(), "panels.yml");

        try {
            init();
        } catch (InvalidConfigurationException e) {
            e.printStackTrace();
        }

        // Dummy
        if (panels.isEmpty()) {
            panels.put("dummy", new PanelConfig());
        }
    }

    @Comment("How many ticks will be between panel view checks (at least 1)")
    private int interval = 1;
    @Comment("Individual panel configurations")
    private Map<String, PanelConfig> panels = new HashMap<>();
}
